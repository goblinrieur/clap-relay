# clap-relay

clap-relay / sound-relay

Sur une idée présentée par Thonain : [vidéo youtube](https://www.youtube.com/watch?v=ZKulTYAQn8Y)

# adaptation

quasiment rien, juste du CMS et du PCB avec des largeurs de pistes adaptées

# Images

![copper](./copper.png)

![composants](./face.png)

# files

[schema](./cr.pdf)

[PCBlayers](./PCBlayers.pdf)

les fichiers gerbers & drill sont dispos ici également.
